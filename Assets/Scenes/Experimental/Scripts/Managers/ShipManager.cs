﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using TMPro;
public class ShipManager : MonoBehaviour
{
	public static ShipManager instance;

	public ShipVariable CurrentShip;

	[Header("Prefabs")]
	public GameObject deckPrefab;
	public GameObject featurePrefab;
	public GameObject overlayPrefab;

	[Header("Spawn Locations")]
	public Transform deckSpawn;
	public Transform featureSpawnTest;
	
	[Header("Color Presets")]
	public Color32 selectedCol;
	public Color32 deselectedCol;

	public Color32 selectedButtonCol;
	public Color32 selectedTextCol;

	public Color32 deSelButtonCol;
	public Color32 deSelTextCol;
	public Color32 deSelDashCol;

	private void Awake()
	{
		for (int i = CurrentShip.Ship.Decks.Length - 1; i > 0; i--)
		{
			CreateDeck(CurrentShip.Ship.Decks[i]);
		}
	}

	/// <summary>
	/// Create a new feature from the prefab and turn it loose on assigning everything else.
	/// </summary>
	/// <param name="parent">Where the feature should spawn.</param>
	public void CreateFeature(Transform parent)
	{
		GameObject newFeatureObject = Instantiate(featurePrefab, parent);
		ShipFeature newFeature = newFeatureObject.transform.GetComponent<ShipFeature>();
		newFeature.AssignFeature(CurrentShip.Ship.Features[0]);
	}

	/// <summary>
	/// Create a new deck from the prefab and turn it loose on assigning everything else.
	/// </summary>
	/// <param name="deck">The deck image, is used to generate the naming of other pieces as well.</param>
	void CreateDeck(Sprite deck)
	{
		GameObject newDeckObject = Instantiate(deckPrefab, deckSpawn);
		DeckSelect newDeckSelect = newDeckObject.GetComponent<DeckSelect>();

		int startIndex = deck.name.LastIndexOf("_");
		string deckName = deck.name.Substring(startIndex + 1);

		newDeckObject.name = "Deck_" + deckName;
		newDeckObject.GetComponentInChildren<TextMeshProUGUI>().text = deckName;
		newDeckObject.transform.Find("Deck").GetComponent<Image>().sprite = deck;
		//TODO: Assign functionality to DeckSelect that set's up all necessary objects based on a truncated string of the
		//image name from the last "_".


	}
}
