﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Created By: Joshua Bowers - 02/22/19
/// Last Edited By: Joshua Bowers - 02/22/19
///
/// Purpose:
/// <summary>

public interface IDeckComponent<T>
{
    
    string DeckID {get;set;}

    bool Equals(T obj);
    
    void Initialize();

    void AssignDeckImage();

    void AssignDeckID();
}