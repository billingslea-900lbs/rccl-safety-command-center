﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Deck : MonoBehaviour, IDeckComponent<Deck>
{
    public string DeckID{get;set;}
    public Image DeckImage;

    public bool Equals(Deck value)
    {
        return value == this;
    }

    public void Initialize()
    {
        
    }

    public void AssignDeckImage()
    {

    }

    public void AssignDeckID()
    {
        
    }
}
