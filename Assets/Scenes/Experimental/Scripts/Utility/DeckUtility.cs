﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Created By: Joshua Bowers - 02/22/19
/// Last Edited By: Joshua Bowers - 02/22/19
///
/// Purpose: To add static functionality for decks/overlays
/// <summary>

public static class DeckUtility
{
    /// <summary>
    /// Simple string method that return the deck assignment
    /// based on the text after the last "_" 
    /// of the inquirer's name (i.e this_overlay_04a == 04a)
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static string GetDeckAssignment(string value)
    {
        int indexOfLastDelimeter = value.LastIndexOf("_") + 1;
        string s = value.Substring(indexOfLastDelimeter);
        return s;
    }
    
    /// <summary>
    /// Method used to find the correct deck transform.
    /// </summary>
    /// <param name="transList">List of all current decks</param>
    /// <param name="value">Inquirer's deck assignment</param>
    /// <returns></returns>
    public static RectTransform FindCorrectDeckTransform(List<RectTransform> transList, string value)
    {
        RectTransform deckTrans = transList.Find(data => ((GetDeckAssignment(data.name) == value)));

        return deckTrans;
    } 
}