﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

using TMPro;
/// <summary>
/// The individual class for all features to be populated.
/// </summary>
/// 
public class ShipFeature : MonoBehaviour
{
	public TextMeshProUGUI featureNameText;

	public List<DeckOverlayComponent> deckOverlay;
	
	/// <summary>
	/// Assigns all of the content to the feature.
	/// </summary>
	/// <param name="featureName">The name of the feature as it will appear on the text object</param>
	/// <param name="decks">Each affected deck</param>
	/// <param name="overlays">Each overlay that corresponds to the above deck array in order.</param>
	public void AssignFeature(FeatureData value)
	{
		featureNameText.text = value.FeatureName;
		//deckOverlay.decks = value.Decks;
		//deckOverlay.overlays = value.Overlays;
	}

	#region Debugging
	[Header("Debugging")]
	public List<RectTransform> debugRects;

	public string debugOverlayName;
	public RectTransform debugSearchingRect;
	private void Update() 
	{
		if(Input.GetKeyDown(KeyCode.Alpha1))
		{
			debugOverlayName = DeckUtility.GetDeckAssignment("SomeRandomDebugging_Overlay_Longview");
			Debug.Log(debugOverlayName, this);
		}

		if(Input.GetKeyDown(KeyCode.Alpha2))
		{
			debugSearchingRect = DeckUtility.FindCorrectDeckTransform(debugRects, debugOverlayName);
			Debug.Log(debugSearchingRect.name + "was found.");
			
		}
		
		
	}
	#endregion
}
