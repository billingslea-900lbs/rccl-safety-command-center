﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

#region Data Classes

/// <summary>
/// Determines an objects place in the features column, this gives the user control to assign specific features to certain columns and populate
/// empty buttons for the rest.
/// </summary>
public enum ColumnID
{
	Left,
	Middle,
	Right
}

[System.Serializable]
public class FeatureData
{
	[SerializeField]
	string featureName;
	public string FeatureName { get { return featureName; } }

	// [SerializeField]
	// List<DeckOverlay> decks;
	// public List<DeckOverlay> Decks { get { return decks; } }

	[SerializeField]
	Sprite[] overlays;
	public Sprite[] Overlays { get { return overlays; } }
}

[System.Serializable]
public class ShipData
{
	[SerializeField]
	string shipName;
	public string ShipName { get { return shipName; } }

	[SerializeField]
	Sprite[] decks;
	public Sprite[] Decks { get { return decks; } }

	[SerializeField]
	FeatureData[] features;
	public FeatureData[] Features { get { return features; } }

	public ShipData(string newShipName, Sprite[] newDecks, FeatureData[] newFeatures)
	{
		shipName = newShipName;
		decks = newDecks;
		features = newFeatures;
	}

}

#endregion

[System.Serializable]
[CreateAssetMenu(menuName = "Ships/New Ship", fileName = "New Ship")]
public class ShipVariable : ScriptableObject
{
	public ShipData Ship = new ShipData("", null, null);

	public void OnValidate()
	{
		if(Ship.ShipName != "" && Ship.Decks != null && Ship.Features != null)
		{
			//Ship.UpdateFeatures();
		}
	}
}
