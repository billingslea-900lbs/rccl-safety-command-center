﻿/*
 * © 900lbs of Creative
 * Creation Date: DATE HERE
 * Date last Modified: MOST RECENT MODIFICATION DATE HERE
 * Name: AUTHOR NAME HERE
 * 
 * Description: DESCRIPTION HERE
 * 
 * Scripts referenced: LIST REFERENCED SCRIPTS HERE
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisableButton : MonoBehaviour 
{
    public Button button;


    private void OnEnable()
    {
        if(button != null)
        {
            button.interactable = true;
        }
        
    }

    // Use this for initialization
    void Start () 
	{
        button = this.GetComponent<Button>();
        button.onClick.AddListener(disableButton);
	}
	
	public void disableButton()
    {
        button.interactable = false;
        StartCoroutine(ReEnableButton());
    }

    public IEnumerator ReEnableButton()
    {
        yield return new WaitForSeconds(0.25f);
        button.interactable = true;
    }
}
