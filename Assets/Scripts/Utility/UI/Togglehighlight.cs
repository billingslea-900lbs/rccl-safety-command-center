﻿/*
 * © 900lbs of Creative
 * Creation Date: DATE HERE
 * Date last Modified: MOST RECENT MODIFICATION DATE HERE
 * Name: AUTHOR NAME HERE
 * 
 * Description: DESCRIPTION HERE
 * 
 * Scripts referenced: LIST REFERENCED SCRIPTS HERE
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Togglehighlight : MonoBehaviour 
{
    Image img;

    TextMeshProUGUI text;

    public Color textSelectedColor;
    public Color textUnselectedColor;
    public Color imgSelectedColor;
    public Color unselectedColor;

    public bool highlighted;

    // Use this for initialization
    void Start()
    {
        img = this.GetComponent<Image>();

        text = this.GetComponentInChildren<TextMeshProUGUI>();

        this.GetComponent<Button>().onClick.AddListener(Highlight);
    }

    public void Highlight()
    {
        if (!highlighted)
        {
            img.color = imgSelectedColor;
            text.color = textSelectedColor;
            highlighted = true;
        }

        else
        {
            img.color = unselectedColor;
            text.color = textUnselectedColor;
            highlighted = false;
        }
    }
}
