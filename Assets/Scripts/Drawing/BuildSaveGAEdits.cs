﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using TMPro;

public class BuildSaveGAEdits : MonoBehaviour
{
    public string fileName;

    public string file;

    public List<string> lines;
    public string[] parts;

    public GameObject newPrefab;
    public GameObject confirmationScreen;

    public DrawOnDeck drawOnDeck;

    public Button resetButton;
    private Image resetFrame;
    private TextMeshProUGUI resetTitle;

    // Use this for initialization
    void Start ()
    {
        resetButton.onClick.AddListener(ConfirmReset);
        resetFrame = resetButton.transform.GetChild(0).GetComponent<Image>();
        resetTitle = resetFrame.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        drawOnDeck.buildSaveGAEdits = this;
        LoadPrefabs();
	}

    public void SavePrefabs()
    {
        lines.Clear();

        for(int i = 0; i < drawOnDeck.gaHolders.Count; i++)
        {
            if(drawOnDeck.gaHolders[i].transform.childCount > 0)
            {
                for(int j = 0; j < drawOnDeck.gaHolders[i].transform.childCount; j++)
                {
                    if(drawOnDeck.gaHolders[i].transform.GetChild(j).name == "UI Line Renderer(Clone)")
                    {
                        NewAnnontationSave newAnnontation = new NewAnnontationSave();

                        UILineRenderer uILineRenderer = drawOnDeck.gaHolders[i].transform.GetChild(j).GetComponent<UILineRenderer>();

                        newAnnontation.prefabName = drawOnDeck.gaHolders[i].transform.GetChild(j).name.Replace("(Clone)", "");
                        newAnnontation.GANumber = i.ToString();
                        newAnnontation.color = "#" + ColorUtility.ToHtmlStringRGB(uILineRenderer.color);
                        newAnnontation.thickness = uILineRenderer.lineThickness.ToString();
                        
                        if(uILineRenderer.gameObject.GetComponent<UIMultiplyEffect>() != null)
                        {
                            newAnnontation.isHighlighter = "true";
                        }

                        else
                        {
                            newAnnontation.isHighlighter = "false";
                        }

                        newAnnontation.points = new string[uILineRenderer.Points.Length];

                        for(int k = 0; k < newAnnontation.points.Length; k++)
                        {
                            newAnnontation.points[k] = uILineRenderer.Points[k].x.ToString() + "=" + uILineRenderer.Points[k].y.ToString();
                        }

                        string lineToAdd = newAnnontation.prefabName + "," + newAnnontation.GANumber + "," + newAnnontation.color + "," +
                            newAnnontation.thickness + "," + newAnnontation.isHighlighter;

                        foreach(string newPoint in newAnnontation.points)
                        {
                            lineToAdd = lineToAdd + "," + newPoint;
                        }

                        lines.Add(lineToAdd);
                    }

                    else
                    {
                        NewIconSave newIcon = new NewIconSave();

                        newIcon.prefabName = drawOnDeck.gaHolders[i].transform.GetChild(j).name.Replace("(Clone)", "");
                        newIcon.GANumber = i.ToString();
                        newIcon.X = drawOnDeck.gaHolders[i].transform.GetChild(j).GetComponent<RectTransform>().anchoredPosition.x.ToString();
                        newIcon.Y = drawOnDeck.gaHolders[i].transform.GetChild(j).GetComponent<RectTransform>().anchoredPosition.y.ToString();
                        newIcon.Z = "0";

                        lines.Add(newIcon.prefabName + "," + newIcon.GANumber + "," + newIcon.X + "," + newIcon.Y + "," + newIcon.Z);
                    }
                }
            }
        }

        WriteOverlaysToCSV();
    }

    public void ConfirmReset()
    {
        confirmationScreen.SetActive(true);
    }

    public void ResetPrefabs()
    {
        for (int i = 0; i < drawOnDeck.gaHolders.Count; i++)
        {
            if (drawOnDeck.gaHolders[i].transform.childCount > 0)
            {
                for (int j = 0; j < drawOnDeck.gaHolders[i].transform.childCount; j++)
                {
                    Destroy(drawOnDeck.gaHolders[i].transform.GetChild(j).gameObject);
                }
            }
        }

        if(lines != null)
        {
            LoadPrefabs();
        }
        
        confirmationScreen.SetActive(false);
    }

    public void ToggleReset(Color newColor)
    {
        resetButton.interactable = !resetButton.interactable;
        resetFrame.color = newColor;
        resetTitle.color = newColor;
    }

    private void LoadPrefabs()
    {
        file =  System.IO.File.ReadAllText(GetPath());

        lines = file.Split('\n').ToList();

        for (int i = 0; i < lines.Count; i++)
        {
            if (lines[i] == "")
            {
                lines.Remove(lines[i]);
            }

            else
            {
                parts = lines[i].Split(","[0]);

                BuildPrefab(parts);
            }
        }
    }

    private void BuildPrefab(string[] info)
    {
        if(info[0] == "UI Line Renderer")
        {
            GameObject prefabToLoad = GetPrefab(info[0]);

            prefabToLoad.transform.SetParent(drawOnDeck.gaHolders[int.Parse(info[1])].transform, false);

            UILineRenderer newLineRenderer = prefabToLoad.GetComponent<UILineRenderer>();

            Color lineColor;

            ColorUtility.TryParseHtmlString(info[2], out lineColor);

            newLineRenderer.color = lineColor;

            newLineRenderer.lineThickness = float.Parse(info[3]);

            if(info[4] == "true")
            {
                prefabToLoad.AddComponent<UIMultiplyEffect>();
            }

            Vector2[] linePoints = new Vector2[info.Length - 5];

            for(int i = 0; i < linePoints.Length; i++)
            {
                string[] vectorPoint = info[i + 5].Split("="[0]);

                linePoints[i].x = float.Parse(vectorPoint[0]);
                linePoints[i].y = float.Parse(vectorPoint[1]);
            }

            newLineRenderer.Points = linePoints;
        }

        else
        {
            GameObject prefabToLoad = GetPrefab(info[0]);

            Vector3 prefabPosition = new Vector3(float.Parse(info[2]), float.Parse(info[3]), float.Parse(info[4]));

            prefabToLoad.transform.SetParent(drawOnDeck.gaHolders[int.Parse(info[1])].transform, false);

            prefabToLoad.GetComponent<RectTransform>().anchoredPosition = prefabPosition;
        }
        
    }

    void WriteOverlaysToCSV()
    {
        string filePath = GetPath();

        StreamWriter writer = new StreamWriter(filePath);

        for (int i = 0; i < lines.Count; i++)
        {
            if (lines[i] != "")
            {
                writer.WriteLine(lines[i]);
            }
        }

        writer.Flush();

        writer.Close();
    }

    private GameObject GetPrefab(string name)
    {
        if(name == "UI Line Renderer")
        {
            newPrefab = Instantiate(Resources.Load("Prefabs/" + name)) as GameObject;
        }

        else
        {
            newPrefab = Instantiate(Resources.Load("Prefabs/SafetyIconPrefabs/" + name)) as GameObject;
        }
        
        return newPrefab;
    }

    private string GetPath()
    {
        return Application.streamingAssetsPath + "/GAOverlay.csv";
    }
}

[System.Serializable]
public class NewIconSave
{
    public string prefabName;
    public string GANumber;
    public string X;
    public string Y;
    public string Z;
}

[System.Serializable]
public class NewAnnontationSave
{
    public string prefabName;
    public string GANumber;
    public string color;
    public string thickness;
    public string isHighlighter;
    public string[] points;
}
