﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TouchScript.Gestures.TransformGestures;

public class IconBehavior : MonoBehaviour
{
    public Button deleteButton;

    public DrawOnDeck master;

    public TransformGesture transGesture;

    public Image thisImg;

    public bool GAEdit;

    void Start ()
    {
        transGesture = this.GetComponent<TransformGesture>();
        master = FindObjectOfType<DrawOnDeck>();
        deleteButton = this.GetComponent<Button>();
        deleteButton.onClick.AddListener(DeleteIcon);
        thisImg = this.GetComponent<Image>();
        transGesture.enabled = false;

        if(transform.parent.name == "GAHolder")
        {
            GAEdit = true;
        }

        else
        {
            GAEdit = false;
        }
    }

    private void Update()
    {
        if (!GAEdit)
        {
            if (master.erase)
            {
                //thisImg.raycastTarget = true;
                deleteButton.interactable = true;
                transGesture.enabled = false;
            }

            if (!master.erase)
            {
                //thisImg.raycastTarget = false;
                deleteButton.interactable = false;
                transGesture.enabled = true;
            }
        }

        else
        {
            if (master.erase)
            {
                //thisImg.raycastTarget = true;
                deleteButton.interactable = true;
                transGesture.enabled = false;
            }

            if (!master.erase)
            {
                //thisImg.raycastTarget = false;
                deleteButton.interactable = false;
                transGesture.enabled = true;
            }
        }
    }

    public void DeleteIcon()
    {
        if(master.erase && this.transform.parent.name == "LineHolder")
        {
            Destroy(this.gameObject);
        }

        if (master.erase && this.transform.parent.name == "GAHolder")
        {
            Destroy(this.gameObject);
        }
    }
}
