﻿/*
 * © 900lbs of Creative
 * Creation Date: DATE HERE
 * Date last Modified: MOST RECENT MODIFICATION DATE HERE
 * Name: AUTHOR NAME HERE
 * 
 * Description: DESCRIPTION HERE
 * 
 * Scripts referenced: LIST REFERENCED SCRIPTS HERE
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions.Examples;
using UnityEngine.UI.Extensions;
using UnityEngine.UI;

public class LineBehavior : MonoBehaviour 
{
    public DrawOnDeck master;

    public bool GAEdit;

    UILineRenderer lineRenderer;

    RectTransform deleteButton_Rect;

    Image thisImg;

    Button deleteButton;

    private float MaxY;
    private float MinY;
    private float MaxX;
    private float MinX;

    private int curPoints;

    private void Start()
    {
        master = FindObjectOfType<DrawOnDeck>();
        lineRenderer = this.GetComponent<UILineRenderer>();

        deleteButton_Rect = transform.GetChild(0).transform.GetComponent<RectTransform>();
        deleteButton = deleteButton_Rect.GetComponent<Button>();
        thisImg = deleteButton_Rect.GetComponent<Image>();

        deleteButton.onClick.AddListener(deleteLine);

        deleteButton.interactable = false;

        if (transform.parent.name == "GAHolder")
        {
            GAEdit = true;
        }

        else
        {
            GAEdit = false;
        }
    }

    private void Update()
    {
        if(lineRenderer.Points.Length > curPoints)
        {
            for (int i = 0; i < lineRenderer.Points.Length; i++)
            {
                if (i == 0)
                {
                    MaxY = lineRenderer.Points[i].y;
                    MinY = lineRenderer.Points[i].y;
                    MaxX = lineRenderer.Points[i].x;
                    MinX = lineRenderer.Points[i].x;
                }

                if (lineRenderer.Points[i].y > MaxY)
                {
                    MaxY = lineRenderer.Points[i].y;
                }

                if (lineRenderer.Points[i].y < MinY)
                {
                    MinY = lineRenderer.Points[i].y;
                }

                if (lineRenderer.Points[i].x > MaxX)
                {
                    MaxX = lineRenderer.Points[i].x;
                }

                if (lineRenderer.Points[i].x < MinX)
                {
                    MinX = lineRenderer.Points[i].x;
                }
            }

            CalculateButton();

            curPoints = lineRenderer.Points.Length;
        }

        if(!GAEdit)
        {
            if (master.erase && !thisImg.raycastTarget)
            {
                thisImg.raycastTarget = true;
                deleteButton.interactable = true;
            }

            if (!master.erase && thisImg.raycastTarget)
            {
                thisImg.raycastTarget = false;
                deleteButton.interactable = false;
            }
        }

        else
        {
            if (master.erase && !thisImg.raycastTarget)
            {
                thisImg.raycastTarget = true;
                deleteButton.interactable = true;
            }

            if (!master.erase && thisImg.raycastTarget)
            {
                thisImg.raycastTarget = false;
                deleteButton.interactable = false;
            }
        }

        
    }

    public void CalculateButton()
    {
        Vector2 position = new Vector2(MinX - 5, MinY - 5);
        deleteButton_Rect.localPosition = position;

        Vector2 size = new Vector2((MaxX - MinX) + 10, (MaxY - MinY) + 10);
        deleteButton_Rect.sizeDelta = size;
    }

    public void deleteLine()
    {
        if(master.erase && this.transform.parent.name == "LineHolder")
        {
            Destroy(this.gameObject);
        }

        if(master.erase && this.transform.parent.name == "GAHolder")
        {
            Destroy(this.gameObject);
        }
    }
}
