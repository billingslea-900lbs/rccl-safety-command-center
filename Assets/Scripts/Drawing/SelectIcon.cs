﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectIcon : MonoBehaviour
{
    public DrawOnDeck drawOnDeck;

    public GameObject iconSelection;

    public Button selectionButton;

    public Image bracket;

	// Use this for initialization
	void Start ()
    {
        drawOnDeck = FindObjectOfType<DrawOnDeck>();
        selectionButton = this.GetComponent<Button>();
        selectionButton.onClick.AddListener(IconPicker);

        bracket = this.transform.GetChild(0).GetComponent<Image>();
	}
	
	public void IconPicker()
    {
        if(drawOnDeck.placeIcon)
        {
            drawOnDeck.toggleHighlighter(drawOnDeck.moveBH.GetComponent<Button>());
        }


        if (!drawOnDeck.placeIcon)
        {
            ToggleBracket();
            drawOnDeck.iconToPlace = iconSelection;
            drawOnDeck.selectIcon = this;
            drawOnDeck.placeIcon = true;
            drawOnDeck.toggleHighlighter(selectionButton);
        }

        
    }

    public void ToggleBracket()
    {
        if(!bracket.enabled)
        {
            bracket.enabled = true;
        }

        else
        {
            bracket.enabled = false;
        }
    }
}
