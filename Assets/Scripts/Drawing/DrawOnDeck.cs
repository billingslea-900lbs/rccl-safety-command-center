﻿using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI.Extensions;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class DrawOnDeck : MonoBehaviour, IPointerDownHandler
{
    #region shit to fix
    public GameObject LineRendererPrefab;
    public UILineRenderer LineRenderer;
    public Transform parentObject;
    public RectTransform parentRect;
    public Color lineCol;

    public GameObject iconToPlace;
    public SelectIcon selectIcon;
    public GameObject newIcon;

    public Button toggleGAEditMode;
    public Button toggleGAOverlay;

    public BuildSaveGAEdits buildSaveGAEdits;

    public GameObject editGABorder;

    public Canvas parentCanvasOfImageToMove;
    public CanvasScaler canvasScaler;
    public Image brush; 

    public List<Transform> deckSelection;
    public List<GameObject> lineHolders;
    public List<GameObject> gaHolders;

    public Button hideButton;
    public GameObject HideButtonGO;

    public ZoomOut zoomOut;

    public Image hideButtonIcon;
    public TextMeshProUGUI hideButtonText;

    public Color inactiveColor;
    public Color activeColor;

    public Button drawButton;
    public GameObject DrawPanel;
    public WidgetToDock widgetToDock;

    public ButtonHighlight highlighterBH;
    public ButtonHighlight penBH;
    public ButtonHighlight eraserBH;
    public ButtonHighlight moveBH;

    public ToggleSwitch toggleAnnotations;
    public ToggleSwitch toggleGAEdits;

    private float Size;

    public bool draw;
    public bool placeIcon;
    private bool startedPlacing;
    private bool editGA;
    public bool erase;
    //public bool eraseGA;
    private bool hidingDecks;
    private bool highlighter;
    public bool rotated;
    private bool widgetActive;

    private Vector3 mousePos;
    private Vector2 pos;

    private GameObject curDeckMap;

    List<Touch> touches;
    #endregion

    void Start()
    {
        zoomOut = FindObjectOfType<ZoomOut>();
        HideButtonGO = hideButton.gameObject;
        inactiveColor = hideButtonIcon.color;
        ChangeThickness(5f);

        SetUpButtons();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("Name: " + eventData.pointerEnter.gameObject.name);
    }

    private void Update()
    {
        if(draw)
        {
            MakeAnnotation();
        }

        if (placeIcon)
        {
            MakeNewIcon();
        }
    }

    public void MakeAnnotation()
    {
        touches = InputHelper.GetTouches();

        if (touches.Count > 0)
        {
            if (touches.Count == 1 && OverObject())
            {
                if (LineRenderer == null)
                {
                    NewLineRenderer();
                }

                else if (LineRenderer != null)
                {
                    UpdateBrushPosition();
                    AddNewPoint(mousePos);
                }
            }

            else if (!OverObject())
            {
                parentObject = null;
                parentRect = null;
            }
        }

        if (touches.Count == 0)
        {
            LineRenderer = null;
            parentObject = null;
            parentRect = null;
            curDeckMap = null;
        }
    }

    public void NewLineRenderer()
    {
        if (!editGA)
        {
            parentObject = ExtendedStandaloneInputModule.GetPointerEventData(Input.touches[0].fingerId).pointerEnter.transform.Find("LineHolder").transform;
        }

        else
        {
            parentObject = ExtendedStandaloneInputModule.GetPointerEventData(Input.touches[0].fingerId).pointerEnter.transform.Find("GAHolder").transform;
        }

        parentRect = parentObject.GetComponent<RectTransform>();
        GameObject newLineRenderer = Instantiate(LineRendererPrefab, parentObject);
        LineRenderer = newLineRenderer.GetComponent<UILineRenderer>();
        newLineRenderer.GetComponent<LineBehavior>().master = this;
        
        if(!editGA)
        {
            newLineRenderer.GetComponent<LineBehavior>().GAEdit = false;
        }

        if(editGA)
        {
            newLineRenderer.GetComponent<LineBehavior>().GAEdit = true;
        }

        if (highlighter)
        {
            newLineRenderer.AddComponent<UIMultiplyEffect>();
            LineRenderer.color = HighLighterColor(lineCol);
        }

        else
        {
            LineRenderer.color = lineCol;
        }

        LineRenderer.lineThickness = Size;
        UpdateBrushPosition();
    }

    public void MakeNewIcon()
    {
        touches = InputHelper.GetTouches();

        if (touches.Count > 0)
        {
            if (touches.Count == 1 && OverObject())
            {
                startedPlacing = true;
                PlaceTheIcon();
            }
        }

        if (touches.Count == 0 && startedPlacing)
        {
            iconToPlace = null;
            placeIcon = false;
            startedPlacing = false;
            newIcon.GetComponent<Image>().raycastTarget = true;
            newIcon.GetComponent<IconBehavior>().transGesture.enabled = true;
            selectIcon.ToggleBracket();
            selectIcon = null;
            newIcon = null;
            toggleHighlighter(moveBH.GetComponent<Button>());
            
        }
    }

    public void PlaceTheIcon()
    {
        if(newIcon == null)
        {
            if(!editGA)
            {
                parentObject = ExtendedStandaloneInputModule.GetPointerEventData(Input.touches[0].fingerId).pointerEnter.transform.Find("LineHolder").transform;
            }

            else
            {
                parentObject = ExtendedStandaloneInputModule.GetPointerEventData(Input.touches[0].fingerId).pointerEnter.transform.Find("GAHolder").transform;
            }

            parentRect = parentObject.GetComponent<RectTransform>();
            newIcon = Instantiate(iconToPlace, parentObject);
            newIcon.GetComponent<IconBehavior>().master = this;
            newIcon.GetComponent<Image>().raycastTarget = false;

            if (!editGA)
            {
                newIcon.GetComponent<IconBehavior>().GAEdit = false;
            }

            if (editGA)
            {
                newIcon.GetComponent<IconBehavior>().GAEdit = true;
            }
        }

        if (!editGA)
        {
            if(parentObject != ExtendedStandaloneInputModule.GetPointerEventData(Input.touches[0].fingerId).pointerEnter.transform.Find("LineHolder").transform)
            {
                parentObject = ExtendedStandaloneInputModule.GetPointerEventData(Input.touches[0].fingerId).pointerEnter.transform.Find("LineHolder").transform;
                parentRect = parentObject.GetComponent<RectTransform>();
                newIcon.transform.parent = parentObject;
            }
            
        }

        else
        {
            if (parentObject != ExtendedStandaloneInputModule.GetPointerEventData(Input.touches[0].fingerId).pointerEnter.transform.Find("GAHolder").transform)
            {
                parentObject = ExtendedStandaloneInputModule.GetPointerEventData(Input.touches[0].fingerId).pointerEnter.transform.Find("GAHolder").transform;
                parentRect = parentObject.GetComponent<RectTransform>();
                newIcon.transform.parent = parentObject;
            }
        }

        UpdateBrushPosition();
        newIcon.transform.position = mousePos;
    }

    public void UpdateBrushPosition()
    {
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvasOfImageToMove.transform as RectTransform, touches[0].position, parentCanvasOfImageToMove.worldCamera, out pos);
        brush.transform.position = parentCanvasOfImageToMove.transform.TransformPoint(pos);
        mousePos = brush.transform.position;
    }

    public void AddNewPoint(Vector3 point)
    {
        var pointlist = new List<Vector2>(LineRenderer.Points);
        Vector3[] corners = new Vector3[4];
        parentRect.GetWorldCorners(corners);

        Vector2 drawPoint = point - corners[0];

        drawPoint = drawPoint / canvasScaler.scaleFactor;

        if(rotated)
        {
            drawPoint = new Vector2(drawPoint.y * -1, drawPoint.x);
        }


        if (LineRenderer.Points[0] == Vector2.zero)
        {
            pointlist.RemoveAt(0);
            pointlist.Add(drawPoint);
        }

        else
        {
            pointlist.Add(drawPoint);
        }

        LineRenderer.Points = pointlist.ToArray();
    }

    public void HideDecks()
    {
        if(!hidingDecks)
        {
            foreach (Transform decks in deckSelection)
            {
                if (!decks.GetComponent<DeckSelect>().deckSelected)
                {
                    decks.gameObject.SetActive(false);
                }

                else
                {
                    continue;
                }
            }

            hidingDecks = true;
        }

        else
        {
            foreach(Transform decks in deckSelection)
            {
                decks.gameObject.SetActive(true);

                if(decks.GetComponent<DeckSelect>().deckSelected)
                {
                    decks.GetComponent<DeckSelect>().selectDeck();
                }                
            }

            hidingDecks = false;

            ChangeButtonColor(inactiveColor, false);
        }

        zoomOut.zoomOut();
    }

    public void ChangeColor(Color color)
    {
        lineCol = color;
    }

    public void ChangeThickness(float thickness)
    {
        Size = thickness;
    }

    public void DrawMode()
    {
        widgetActive = !widgetActive;

        if (!widgetActive)
        {
            toggleHighlighter(moveBH.GetComponent<Button>());
            widgetToDock.ButtonColorChange(widgetToDock.inactiveColor, false);
        }

        DrawPanel.transform.SetSiblingIndex(DrawPanel.transform.parent.childCount - 1);

        DrawPanel.SetActive(widgetActive);

        if(DrawPanel.activeSelf)
        {
            widgetToDock.DockWidget();
            widgetToDock.visible = true;
        }
        
        if(!DrawPanel.activeSelf)
        {
            widgetToDock.UnDockWidget();
        }
    }

    public void toggleHighlighter(Button button)
    {
        if(button == highlighterBH.GetComponent<Button>())
        {
            highlighterBH.Highlight(true);
            penBH.Highlight(false);
            eraserBH.Highlight(false);
            moveBH.Highlight(false);
            highlighter = true;
            draw = true;
            placeIcon = false;
            //eraseGA = false;
            erase = false;

            if (selectIcon != null)
            {
                selectIcon.ToggleBracket();
                iconToPlace = null;
                selectIcon = null;
            }
        }

        if (button == penBH.GetComponent<Button>())
        {
            penBH.Highlight(true);
            highlighterBH.Highlight(false);
            eraserBH.Highlight(false);
            moveBH.Highlight(false);
            highlighter = false;
            draw = true;
            placeIcon = false;
            //eraseGA = false;
            erase = false;

            if (selectIcon != null)
            {
                selectIcon.ToggleBracket();
                iconToPlace = null;
                selectIcon = null;
            }
        }

        if (button == eraserBH.GetComponent<Button>())
        {
            eraserBH.Highlight(true);
            penBH.Highlight(false);
            highlighterBH.Highlight(false);
            moveBH.Highlight(false);
            highlighter = false;
            draw = false;
            placeIcon = false;

            //if(editGA)
            //{
            //    eraseGA = true;
            //}

            //if(!editGA)
            //{
                erase = true;
            //}
            

            if (selectIcon != null)
            {
                selectIcon.ToggleBracket();
                iconToPlace = null;
                selectIcon = null;
            }
        }

        if (button == moveBH.GetComponent<Button>())
        {
            if (selectIcon != null)
            {
                selectIcon.ToggleBracket();
                iconToPlace = null;
                selectIcon = null;
            }

            moveBH.Highlight(true);
            penBH.Highlight(false);
            highlighterBH.Highlight(false);
            eraserBH.Highlight(false);
            highlighter = false;
            draw = false;
            placeIcon = false;
            //eraseGA = false;
            erase = false;

            
        }

        else if (selectIcon != null && button == selectIcon.selectionButton)
        {
            moveBH.Highlight(false);
            penBH.Highlight(false);
            highlighterBH.Highlight(false);
            eraserBH.Highlight(false);
            highlighter = false;
            draw = false;
            erase = false;
            //eraseGA = false;
        }

    }

    public void ToggleEditGAMode()
    {
        if(!editGA)
        {
            if (!widgetActive)
            {
                DrawMode();
                drawButton.GetComponent<ToggleSwitch>().Toggle();
            }

            if(!toggleGAEdits.toggled)
            {
                toggleGAEdits.Toggle();
            }

            foreach (GameObject lh in lineHolders)
            {
                lh.SetActive(false);
            }

            buildSaveGAEdits.ToggleReset(activeColor);

            placeIcon = false;
            draw = false;
            editGA = true;
            
        }
        
        else
        {
            if (widgetActive)
            {
                DrawMode();
                drawButton.GetComponent<ToggleSwitch>().Toggle();
            }

            foreach (GameObject lh in lineHolders)
            {
                lh.SetActive(true);
            }

            placeIcon = false;
            draw = false;
            editGA = false;

            buildSaveGAEdits.ToggleReset(inactiveColor);

            buildSaveGAEdits.SavePrefabs();
        }

        editGABorder.SetActive(editGA);
    }

    public void eraseAllAnnotations()
    {
        if(!editGA)
        {
            foreach (GameObject lh in lineHolders)
            {
                for (int i = 0; i < lh.transform.childCount; i++)
                {
                    Destroy(lh.transform.GetChild(i).gameObject);
                }
            }
        }

        else
        {
            foreach (GameObject gah in gaHolders)
            {
                for (int i = 0; i < gah.transform.childCount; i++)
                {
                    Destroy(gah.transform.GetChild(i).gameObject);
                }
            }
        }
        
    }

    public void ToggleArtwork()
    {
        foreach(GameObject lh in lineHolders)
        {
            lh.SetActive(toggleAnnotations.toggled);
        }
    }

    public void ToggleGAEdits()
    {
        foreach (GameObject gah in gaHolders)
        {
            gah.SetActive(toggleGAEdits.toggled);
        }
    }

    public void ChangeButtonColor(Color newColor, bool state)
    {
        HideButtonGO.SetActive(state);
        hideButtonIcon.DOColor(newColor, 0.125f);
        hideButtonText.DOColor(newColor, 0.125f);
    }

    public void SetUpButtons()
    {
        hideButton.onClick.AddListener(HideDecks);
        drawButton.onClick.AddListener(DrawMode);
        toggleGAEditMode.onClick.AddListener(ToggleEditGAMode);

        highlighterBH.GetComponent<Button>().onClick.AddListener(delegate {
            toggleHighlighter(highlighterBH.GetComponent<Button>());
        });

        penBH.GetComponent<Button>().onClick.AddListener(delegate {
            toggleHighlighter(penBH.GetComponent<Button>());
        });

        eraserBH.GetComponent<Button>().onClick.AddListener(delegate {
            toggleHighlighter(eraserBH.GetComponent<Button>());
        });

        moveBH.GetComponent<Button>().onClick.AddListener(delegate {
            toggleHighlighter(moveBH.GetComponent<Button>());
        });
    }

    public Color HighLighterColor(Color initialColor)
    {
        float H, S, V;

        Color.RGBToHSV(initialColor, out H, out S, out V);

        S = S - 0.4f;

        initialColor = Color.HSVToRGB(H, S, V);

        initialColor.a = .75f;

        return initialColor;
    }

    public bool OverObject()
    {
        if (ExtendedStandaloneInputModule.GetPointerEventData(Input.touches[0].fingerId).pointerEnter != null)
        {
            if(ExtendedStandaloneInputModule.GetPointerEventData(Input.touches[0].fingerId).pointerEnter.gameObject == curDeckMap)
            {
                return true;
            }

            else
            {
                if(ExtendedStandaloneInputModule.GetPointerEventData(Input.touches[0].fingerId).pointerEnter.name.StartsWith("DeckMap"))
                {
                    curDeckMap = ExtendedStandaloneInputModule.GetPointerEventData(Input.touches[0].fingerId).pointerEnter.gameObject;
                }
                
                else
                {
                    curDeckMap = null;
                }

                LineRenderer = null;
                return false;
            }
        }

        else
        {
            curDeckMap = null;
            LineRenderer = null;

            return false;
        }
    }

    public bool CheckTheDecks()
    {
        foreach (Transform decks in deckSelection)
        {
            if (decks.GetComponent<DeckSelect>().deckSelected || !decks.gameObject.activeSelf)
            {
                return true;
            }
        }

        return false;
    }
}
