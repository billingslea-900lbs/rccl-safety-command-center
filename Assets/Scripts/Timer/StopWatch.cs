﻿/*
 * © 900lbs of Creative
 * Creation Date: DATE HERE
 * Date last Modified: MOST RECENT MODIFICATION DATE HERE
 * Name: AUTHOR NAME HERE
 * 
 * Description: DESCRIPTION HERE
 * 
 * Scripts referenced: LIST REFERENCED SCRIPTS HERE
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StopWatch : MonoBehaviour 
{
    private string timedis;

    private Button startButton;
    private Button stopButton;
    private Button resetButton;
    private Button selectButton;

    private ButtonHighlight2 resetButtonHighlight;

    private float initialSeconds;
    private int initalMinutes;
    private int initalHours;

    private bool alert1;
    private bool alert2;
    private bool alert3;

    public RectTransform textBG;

    public Image textBGImage;

    public CreateCustomTimer createCustomTimer;

    public ToggleMenu toggleMenu;

    public AlertTime alert1Time;
    public AlertTime alert2Time;
    public AlertTime alert3Time;

    public float sec;
    public int min;
    public int hou;

    public TextMeshProUGUI timeText;
    public TextMeshProUGUI titleText;

    public Color initialColor;
    public Color alert1Color;
    public Color alert2Color;
    public Color alert3Color;
    public Color selectedColor;
    public Color deselectedColor;

    public bool startime = false;
    public bool isStopWatch;
    public bool selected;

    void Start()
    {
        toggleMenu = FindObjectOfType<ToggleMenu>();
        SetUpButtons();

        if (isStopWatch)
        {
            sec = 0;
            min = 0;
            hou = 0;
        }

        initialSeconds = sec;
        initalMinutes = min;
        initalHours = hou;

        initialColor = timeText.color;

        

        if(titleText != null)
        {
            this.gameObject.name = titleText.text;
            deselectedColor = titleText.color;
        }

        StartCoroutine(SetTextBG());
        
        PopulateTime();
    }

    void Update()
    {
        if (startime == true)
        {
            if(isStopWatch)
            {
                StopWatchTimer();
            }

            else
            {
                CountDownTimer();
            }

            PopulateTime();
        }
    }

    public void StopWatchTimer()
    {
        sec += Time.deltaTime;

        if (Mathf.Floor(sec) >= 60)
        {
            sec = 0; min = min + 1;
        }

        if (min >= 60)
        {
            min = 0; hou = hou + 1;
        }
    }

    public void CountDownTimer()
    {
        sec -= Time.deltaTime;

        if (Mathf.Floor(sec) < 0)
        {
            if(min > 0 || hou > 0)
            {
                sec = 60;
                min = min - 1;
            }
            
            else
            {
                sec = 0;
                StopTimer();
            }
        }

        if (min < 0)
        {
            if(hou > 0)
            {
                min = 59;
                hou = hou - 1;
            }
            
            else
            {
                min = 0;
            }
        }

        CheckAlert();
    }

    public void PopulateTime()
    {
        timedis = "[" + (hou.ToString("00") + ":" + min.ToString("00") + ":" + Mathf.Floor(sec).ToString("00")) + "]";

        timeText.text = timedis;
    }

    public void CheckAlert()
    {
        //Make this easier to read
        if(!alert1)
        {
            if (hou <= alert1Time.alertHours)
            {
                if (min <= alert1Time.alertMinutes)
                {
                    if (sec <= alert1Time.alertSeconds + 1)
                    {
                        timeText.color = alert1Color;
                        alert1 = true;
                    }
                }
            }
        }

        if (!alert2 && alert1)
        {
            if (hou <= alert2Time.alertHours)
            {
                if (min <= alert2Time.alertMinutes)
                {
                    if (sec <= alert2Time.alertSeconds + 1)
                    {
                        timeText.color = alert2Color;
                        alert2 = true;
                    }
                }
            }
        }

        if (!alert3 && alert2)
        {
            if (hou <= alert3Time.alertHours)
            {
                if (min <= alert3Time.alertMinutes)
                {
                    if (sec <= alert3Time.alertSeconds + 1)
                    {
                        timeText.color = alert3Color;
                        alert3 = true;
                    }
                }
            }
        }
    }

    public void ResetTime()
    {
        startime = false;
        sec = initialSeconds;
        min = initalMinutes;
        hou = initalHours;
        timeText.color = initialColor;
        alert1 = false;
        alert2 = false;
        alert3 = false;
        PopulateTime();
    }

    public void StartTimer()
    {
        startime = true;
    }

    public void StopTimer()
    {
        startime = false;
    }

    public void SelectTimer()
    {
        if (!selected)
        {
            if(toggleMenu.mainMenuVisible)
            {
                toggleMenu.ToggleMenus();
            }

            createCustomTimer.selectedTimer = this.gameObject;
            createCustomTimer.scrollMenuButton.RightScroll();
            createCustomTimer.addAlertPanel.SetActive(false);
            createCustomTimer.saveButton.gameObject.SetActive(false);
            createCustomTimer.newTitle.text = this.gameObject.name;
            createCustomTimer.newTitle.enabled = false;
            titleText.color = selectedColor;
            textBGImage.enabled = true;
            selected = true;
        }
        
        else
        {
            createCustomTimer.selectedTimer = null;
            createCustomTimer.scrollMenuButton.LeftScroll();
            createCustomTimer.addAlertPanel.SetActive(true);
            createCustomTimer.saveButton.gameObject.SetActive(true);
            createCustomTimer.newTitle.text = null;
            createCustomTimer.newTitle.enabled = true;
            titleText.color = deselectedColor;
            textBGImage.enabled = false;
            selected = false;
        }
    }

    private void SetUpButtons()
    {
        startButton = transform.Find("Buttons").Find("StartButton").GetComponent<Button>();
        stopButton = transform.Find("Buttons").Find("StopButton").GetComponent<Button>();
        resetButton = transform.Find("Buttons").Find("ResetButton").GetComponent<Button>();

        if(titleText != null)
        {
            if(titleText.gameObject.GetComponent<Button>() != null)
            {
                selectButton = titleText.gameObject.GetComponent<Button>();
                selectButton.onClick.AddListener(SelectTimer);
            }
        }

        resetButtonHighlight = resetButton.GetComponent<ButtonHighlight2>();

        startButton.onClick.AddListener(StartTimer);
        stopButton.onClick.AddListener(StopTimer);
        resetButton.onClick.AddListener(ResetTime);
    }

    public IEnumerator SetTextBG()
    {
        yield return new WaitForSeconds(0.5f);
        if(textBG != null)
        {
            textBG.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, titleText.rectTransform.rect.width + 8);
        }
    }
}

[System.Serializable]
public class AlertTime
{
    public float alertSeconds;
    public int alertMinutes;
    public int alertHours;
}
