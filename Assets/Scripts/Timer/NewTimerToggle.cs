﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NewTimerToggle : MonoBehaviour
{
    public TextMeshProUGUI hours;
    public TextMeshProUGUI mins;
    public TextMeshProUGUI secs;
    public TextMeshProUGUI titleButtonText;

    public TextMeshProUGUI[] hoursChildren;
    public TextMeshProUGUI[] minsChildren;
    public TextMeshProUGUI[] secsChildren;

    public Transform hoursHolder;
    public Transform minsHolder;
    public Transform secsHolder;

    public ScrollRect hoursScroller;
    public ScrollRect minsScroller;
    public ScrollRect secsScroller;

    public HideTimerWidget customTimerWidget;
    public ToggleSwitch timerWidgetToggle;

    public Image addFrameImage;
    public Image addIconImage;

    public Button createNewTimerButton;

    public bool toggled;

    public Color activeColor;
    public Color titleInactiveColor;
    public Color inactiveColor;

	// Use this for initialization
	void Start ()
    {
        hoursChildren = hoursHolder.GetComponentsInChildren<TextMeshProUGUI>();
        minsChildren = minsHolder.GetComponentsInChildren<TextMeshProUGUI>();
        secsChildren = secsHolder.GetComponentsInChildren<TextMeshProUGUI>();

        inactiveColor = hoursChildren[0].color;

        createNewTimerButton = this.GetComponent<Button>();

        createNewTimerButton.onClick.AddListener(ToggleTimerInput);
	}

    public void ToggleTimerInput()
    {
        if(!toggled)
        {
            ChangeColors(activeColor, activeColor);
            hoursScroller.enabled = true;
            minsScroller.enabled = true;
            secsScroller.enabled = true;
            
            if(!customTimerWidget.timerOn)
            {
                customTimerWidget.enableTheTimer();
                timerWidgetToggle.Toggle();
            }

            toggled = !toggled;
        }

        else
        {
            ChangeColors(inactiveColor, titleInactiveColor);
            hoursScroller.enabled = false;
            minsScroller.enabled = false;
            secsScroller.enabled = false;
            toggled = !toggled;
        }
    }
	
	void ChangeColors(Color newColor, Color titleColor)
    {
        foreach(TextMeshProUGUI tmp in hoursChildren)
        {
            tmp.color = newColor;
        }

        foreach (TextMeshProUGUI tmp in minsChildren)
        {
            tmp.color = newColor;
        }

        foreach (TextMeshProUGUI tmp in secsChildren)
        {
            tmp.color = newColor;
        }

        hours.color = newColor;
        mins.color = newColor;
        secs.color = newColor;
        titleButtonText.color = titleColor;
        addFrameImage.color = titleColor;
        addIconImage.color = titleColor;
    }
}
