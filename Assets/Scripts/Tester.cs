﻿/*
 * © 900lbs of Creative
 * Creation Date: DATE HERE
 * Date last Modified: MOST RECENT MODIFICATION DATE HERE
 * Name: AUTHOR NAME HERE
 * 
 * Description: DESCRIPTION HERE
 * 
 * Scripts referenced: LIST REFERENCED SCRIPTS HERE
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using UnityEngine.UI;
using UnityEngine.Networking;
using TMPro;

public class Tester : MonoBehaviour 
{
    public string tokenURL = "https://auth.eniram.fi/auth/realms/master/protocol/openid-connect/token";
    public string endAPIURL = "https://api.eniram.fi/data/api/v1/data/list?";
    public string shipID = "id=1031-011";
    public string apiCallURL;
    public string startTime;
    public string endTime;

    public string username = "sbillingslea@rccl.com";
    public string password = "Cutlass69";

    public TextMeshProUGUI SOG;
    public TextMeshProUGUI STW;
    public TextMeshProUGUI windDirectionText;
    public TextMeshProUGUI windSpeedText;

    public Image dirArrow;
    public Image ship;

    Vector3 newHeading;
    Vector3 newWindDir;

    public bool initialize;

    public infoJson info;
    TokenClassName json;

    public string[] expressions;

	// Use this for initialization
	void Start () 
	{
        generateURL();
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

    public void generateURL()
    {
        apiCallURL = endAPIURL + shipID;
        addExpressions(expressions);
    }

    public void addExpressions(string[] exps)
    {
        foreach(string exp in exps)
        {
            apiCallURL += "&expression=" + exp;
        }

        addTime();
    }

    public void addTime()
    {
        startTime = RoundDown(DateTime.Now, TimeSpan.FromSeconds(30));
        startTime = startTime.Replace(":", "%3A");
        endTime = RoundUp(DateTime.Now, TimeSpan.FromSeconds(30));
        endTime = endTime.Replace(":", "%3A");

        apiCallURL += "&start=" + startTime + "&end=" + endTime + "&period=native";

        Debug.Log(apiCallURL);

        StartCoroutine(GetAccessToken());
    }

    IEnumerator GetAccessToken()
    {
        Dictionary<string, string> content = new Dictionary<string, string>();
        //Fill key and value
        content.Add("grant_type", "password");
        content.Add("client_id", "eniram-data-api");
        content.Add("username", "sbillingslea@rccl.com"); //lmarcano@rccl.com
        content.Add("password", "Cutlass69");  //Adrian04!

        UnityWebRequest www = UnityWebRequest.Post(tokenURL, content);

        CustomCertificate customCertificate = new CustomCertificate();

        www.certificateHandler = customCertificate;
        //Send request
        yield return www.SendWebRequest();

        if (!www.isNetworkError)
        {
            string resultContent = www.downloadHandler.text;
            json = JsonUtility.FromJson<TokenClassName>(resultContent);

            Debug.Log(resultContent);
            StartCoroutine(GetData());

            www.disposeDownloadHandlerOnDispose = true;
            www.disposeCertificateHandlerOnDispose = true;
            www.Dispose();
        }
        else
        {
            Debug.Log("Error!");

            www.disposeDownloadHandlerOnDispose = true;
            www.disposeCertificateHandlerOnDispose = true;
            www.Dispose();
        }
    }

    IEnumerator GetData()
    {
        Dictionary<string, string> content = new Dictionary<string, string>();

        UnityWebRequest www = UnityWebRequest.Post(apiCallURL, content);

        www.SetRequestHeader("Authorization", "Bearer " + json.access_token);
        www.SetRequestHeader("Accept", "application/json");
        www.SetRequestHeader("Content-type", "application/json");

        yield return www.SendWebRequest();
        Debug.Log("Request sent");

        if (!www.isNetworkError)
        {
            string resultContent = www.downloadHandler.text;

            resultContent = resultContent.Replace("[", "");
            resultContent = resultContent.Replace("]", "");

            Debug.Log(resultContent);

            resultContent = Formatter(resultContent);

            checkData(resultContent);

            www.disposeDownloadHandlerOnDispose = true;
            www.disposeCertificateHandlerOnDispose = true;
            www.Dispose();
        }
        else
        {
            Debug.Log("Error!");

            www.disposeDownloadHandlerOnDispose = true;
            www.disposeCertificateHandlerOnDispose = true;
            www.Dispose();
        }
    }

    string RoundDown(DateTime DT, TimeSpan TS)
    {
        var delta = DT.Ticks % TS.Ticks;
        return new DateTime(DT.Ticks - delta, DT.Kind).ToString("yyyy-MM-ddTHH:mm:ss") + ".000";
    }

    string RoundUp(DateTime dt, TimeSpan d)
    {
        var modTicks = dt.Ticks % d.Ticks;
        var delta = modTicks != 0 ? d.Ticks - modTicks : 0;
        return new DateTime(dt.Ticks + delta, dt.Kind).ToString("yyyy-MM-ddTHH:mm:ss") + ".000";
    }

    string Formatter(string stg)
    {
        if (stg.Contains("speed.wind.true"))
        {
            string newString = stg.Replace("speed.wind.true", "windSpeed");
            newString = newString.Replace("direction.wind.true", "windDirection");
            return newString;
        }

        return stg;
    }

    public void checkData(string jsonResult)
    {
        info = JsonUtility.FromJson<infoJson>(jsonResult);

        Debug.Log(info);

        windSpeedText.text = info.windSpeed.ToString("#0.0") + "<size=16> kn</size>";
        windDirectionText.text = info.windDirection.ToString("#00.0");
        SOG.text = info.sog.ToString("#0.0") + "<size=16> kt</size>";
        if(info.stw < 0)
        {
            info.stw = 0;
        }
        STW.text = info.stw.ToString("#0.0") + "<size=16> kt</size>";

        newHeading.Set(0, 0, (float)info.heading * -1);
        ship.rectTransform.eulerAngles = newHeading;

        newWindDir.Set(0, 0, (float)info.windDirection * -1);
        dirArrow.rectTransform.eulerAngles = newWindDir;

        if(!initialize)
        {
            initialize = true;
        }

        info = null;
    }

    public void pingData()
    {
        generateURL();//StartCoroutine(GetAccessToken());
    }
}

[Serializable]
public class TokenClassName
{
    public string access_token;
}

[Serializable]
public class infoJson
{
    public double windSpeed;
    public double windDirection;
    public double heading;
    public double stw;
    public double sog;
}

[Serializable]
public class RootObject
{
    public infoJson[] info;
}