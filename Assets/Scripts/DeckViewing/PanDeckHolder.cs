﻿/*
 * © 900lbs of Creative
 * Creation Date: DATE HERE
 * Date last Modified: MOST RECENT MODIFICATION DATE HERE
 * Name: AUTHOR NAME HERE
 * 
 * Description: DESCRIPTION HERE
 * 
 * Scripts referenced: LIST REFERENCED SCRIPTS HERE
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TouchScript.Gestures.TransformGestures;
using TouchScript.Behaviors;

public class PanDeckHolder : MonoBehaviour
{
    public ScreenTransformGesture screenTransformGesture;
    public Transformer transformer;

    public CanvasScaler canvasScaler;

    public GameObject eventSystem;

    public DrawOnDeck drawOnDeck;
    public ZoomOut zoomOut;

    public RectTransform selectionHolder;

    Vector3 newPos;

    public float multiplier;

    private void Start()
    {
        zoomOut = FindObjectOfType<ZoomOut>();
    }

    //TODO Fix positioning of selectionHolder
    void Update () 
    {
        if(!drawOnDeck.draw && !drawOnDeck.erase && !drawOnDeck.placeIcon && Input.touchCount < 2)
        {
            //eventSystem.GetComponent<TouchScript.Layers.UI.TouchScriptInputModule>().enabled = true;
            screenTransformGesture.enabled = true;
            transformer.enabled = true;

            screenTransformGesture.multiplier = multiplier;
        }

        else
        {
            //eventSystem.GetComponent<TouchScript.Layers.UI.TouchScriptInputModule>().enabled = false;
            screenTransformGesture.enabled = false;
            transformer.enabled = false;
        }
    }
}
