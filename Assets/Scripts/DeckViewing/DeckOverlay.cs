﻿/*
 * © 900lbs of Creative
 * Creation Date: DATE HERE
 * Date last Modified: MOST RECENT MODIFICATION DATE HERE
 * Name: AUTHOR NAME HERE
 * 
 * Description: DESCRIPTION HERE
 * 
 * Scripts referenced: LIST REFERENCED SCRIPTS HERE
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckOverlay : MonoBehaviour 
{
    public bool safetyOverlays;
    public bool doorOverlays;
    public bool assemblyOverlays;
    public bool damageControlPlanOverlays;
    public bool escapeRoutesOverlays;
    public bool hatchesOverlays;
    public bool structuralFireProtectionOverlays;
    public bool waterTightOverlays;
    public bool fireControlOverlays;

    public bool[] overlays;

}
