﻿using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI.Extensions;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class DrawOnDeck : MonoBehaviour, IPointerDownHandler
{
    public GameObject LineRendererPrefab;
    public UILineRenderer LineRenderer;
    public Transform parentObject;
    public RectTransform parentRect;
    public Color lineCol;

    public GameObject iconToPlace;
    public GameObject newIcon;

    public Button toggleGAEditMode;
    public Button toggleGAOverlay;

    public BuildSaveGAEdits buildSaveGAEdits;

    public GameObject editGABorder;

    public Canvas parentCanvasOfImageToMove;
    public CanvasScaler canvasScaler;
    public Image brush; 

    public List<Transform> deckSelection;
    public List<GameObject> lineHolders;
    public List<GameObject> gaHolders;

    public Button hideButton;
    public GameObject HideButtonGO;

    public Image hideButtonIcon;
    public TextMeshProUGUI hideButtonText;

    public Color inactiveColor;
    public Color activeColor;

    public Button drawButton;
    public GameObject DrawPanel;
    public WidgetToDock widgetToDock;

    public ButtonHighlight highlighterBH;
    public ButtonHighlight penBH;
    public ButtonHighlight eraserBH;
    public ButtonHighlight moveBH;

    public ToggleSwitch toggleAnnotations;
    public ToggleSwitch toggleGAEdits;

    public float Size;

    public bool draw;
    public bool placeIcon;
    public bool startedPlacing;
    public bool editGA;
    public bool erase;
    public bool hidingDecks;
    public bool highlighter;
    public bool rotated;
    public bool widgetActive;

    private Vector3 mousePos;
    private Vector2 pos;

    public GameObject curDeckMap;

    List<Touch> touches;

    void Start()
    {
        
        HideButtonGO = hideButton.gameObject;
        inactiveColor = hideButtonIcon.color;
        ChangeThickness(5f);

        SetUpButtons();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("Name: " + eventData.pointerEnter.gameObject.name);
    }

    private void Update()
    {
        if(draw)
        {
            touches = InputHelper.GetTouches();

            if(touches.Count > 0)
            {
                if (touches.Count == 1 && OverObject())
                {
                    if (LineRenderer == null)
                    {
                        NewLineRenderer();
                    }

                    else if (LineRenderer != null)
                    {
                        UpdateBrushPosition();
                        AddNewPoint(mousePos);
                    }
                }

                else if(!OverObject())
                {
                    parentObject = null;
                    parentRect = null;
                }
            }
            
            if (touches.Count == 0)
            {
                LineRenderer = null;
                parentObject = null;
                parentRect = null;
                curDeckMap = null;
            }
        }

        if (placeIcon)
        {
            touches = InputHelper.GetTouches();

            if (touches.Count > 0)
            {
                if (touches.Count == 1 && OverObject())
                {
                    startedPlacing = true;
                    PlaceTheIcon();
                }
            }

            if (touches.Count == 0 && startedPlacing)
            {
                iconToPlace = null;
                placeIcon = false;
                startedPlacing = false;
                newIcon.GetComponent<Image>().raycastTarget = true;
                newIcon = null;
            }
        }
    }

    public void PlaceTheIcon()
    {
        if(newIcon == null)
        {
            if(!editGA)
            {
                parentObject = ExtendedStandaloneInputModule.GetPointerEventData(Input.touches[0].fingerId).pointerEnter.transform.Find("LineHolder").transform;
            }

            else
            {
                parentObject = ExtendedStandaloneInputModule.GetPointerEventData(Input.touches[0].fingerId).pointerEnter.transform.Find("GAHolder").transform;
            }

            parentRect = parentObject.GetComponent<RectTransform>();
            newIcon = Instantiate(iconToPlace, parentObject);
            newIcon.GetComponent<IconBehavior>().master = this;
            newIcon.GetComponent<Image>().raycastTarget = false;
        }

        UpdateBrushPosition();
        newIcon.transform.position = mousePos;
    }

    public void AddNewPoint(Vector3 point)
    {
        var pointlist = new List<Vector2>(LineRenderer.Points);
        Vector3[] corners = new Vector3[4];
        parentRect.GetWorldCorners(corners);

        Vector2 drawPoint = point - corners[0];

        drawPoint = drawPoint / canvasScaler.scaleFactor;

        if(rotated)
        {
            drawPoint = new Vector2(drawPoint.y * -1, drawPoint.x);
        }


        if (LineRenderer.Points[0] == Vector2.zero)
        {
            pointlist.RemoveAt(0);
            pointlist.Add(drawPoint);
        }

        else
        {
            pointlist.Add(drawPoint);
        }

        LineRenderer.Points = pointlist.ToArray();
    }

    public void NewLineRenderer()
    {
        if (!editGA)
        {
            parentObject = ExtendedStandaloneInputModule.GetPointerEventData(Input.touches[0].fingerId).pointerEnter.transform.Find("LineHolder").transform;
        }

        else
        {
            parentObject = ExtendedStandaloneInputModule.GetPointerEventData(Input.touches[0].fingerId).pointerEnter.transform.Find("GAHolder").transform;
        }

        parentRect = parentObject.GetComponent<RectTransform>();
        GameObject newLineRenderer = Instantiate(LineRendererPrefab, parentObject);
        LineRenderer = newLineRenderer.GetComponent<UILineRenderer>();
        newLineRenderer.GetComponent<LineBehavior>().master = this;

        if (highlighter)
        {
            newLineRenderer.AddComponent<UIMultiplyEffect>();
            LineRenderer.color = HighLighterColor(lineCol);
        }

        else
        {
            LineRenderer.color = lineCol;
        }

        LineRenderer.lineThickness = Size;
        UpdateBrushPosition();
    }

    public bool CheckTheDecks()
    {
        foreach(Transform decks in deckSelection)
        {
            if (decks.GetComponent<DeckSelect>().deckSelected || !decks.gameObject.activeSelf)
            {
                return true;
            }
        }

        return false;
    }

    public void HideDecks()
    {
        if(!hidingDecks)
        {
            foreach (Transform decks in deckSelection)
            {
                if (!decks.GetComponent<DeckSelect>().deckSelected)
                {
                    decks.gameObject.SetActive(false);
                }

                else
                {
                    continue;
                }
            }

            hidingDecks = true;
        }

        else
        {
            foreach(Transform decks in deckSelection)
            {
                decks.gameObject.SetActive(true);
                decks.GetComponent<DeckSelect>().deckSelected = false;
            }

            hidingDecks = false;
        }
    }

    public void ClearPoints()
    {
        LineRenderer.Points = new Vector2[0];
    }

    public void ChangeColor(Color color)
    {
        lineCol = color;
    }

    public void ChangeThickness(float thickness)
    {
        Size = thickness;
    }

    public void UpdateBrushPosition()
    {
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvasOfImageToMove.transform as RectTransform, touches[0].position, parentCanvasOfImageToMove.worldCamera, out pos);
        brush.transform.position = parentCanvasOfImageToMove.transform.TransformPoint(pos);
        mousePos = brush.transform.position;
    }

    public void DrawMode()
    {
        widgetActive = !widgetActive;

        if (!widgetActive)
        {
            toggleHighlighter(moveBH.GetComponent<Button>());
            widgetToDock.ButtonColorChange(widgetToDock.inactiveColor, false);
        }

        DrawPanel.transform.SetSiblingIndex(DrawPanel.transform.parent.childCount - 1);

        DrawPanel.SetActive(widgetActive);

        if(DrawPanel.activeSelf)
        {
            widgetToDock.DockWidget();
            widgetToDock.visible = true;
        }
        
        if(!DrawPanel.activeSelf)
        {
            widgetToDock.UnDockWidget();
        }
    }

    public void toggleHighlighter(Button button)
    {
        if(button == highlighterBH.GetComponent<Button>())
        {
            highlighterBH.Highlight(true);
            penBH.Highlight(false);
            eraserBH.Highlight(false);
            moveBH.Highlight(false);
            highlighter = true;
            draw = true;
            placeIcon = false;
            iconToPlace = null;
            erase = false;
        }

        if (button == penBH.GetComponent<Button>())
        {
            penBH.Highlight(true);
            highlighterBH.Highlight(false);
            eraserBH.Highlight(false);
            moveBH.Highlight(false);
            highlighter = false;
            draw = true;
            placeIcon = false;
            iconToPlace = null;
            erase = false;
        }

        if (button == eraserBH.GetComponent<Button>())
        {
            eraserBH.Highlight(true);
            penBH.Highlight(false);
            highlighterBH.Highlight(false);
            moveBH.Highlight(false);
            highlighter = false;
            draw = false;
            placeIcon = false;
            iconToPlace = null;
            erase = true;
        }

        if (button == moveBH.GetComponent<Button>())
        {
            moveBH.Highlight(true);
            penBH.Highlight(false);
            highlighterBH.Highlight(false);
            eraserBH.Highlight(false);
            highlighter = false;
            draw = false;
            placeIcon = false;
            iconToPlace = null;
            erase = false;
        }
    }

    public void ToggleEditGAMode()
    {
        if(!editGA)
        {
            if (!widgetActive)
            {
                DrawMode();
            }

            foreach (GameObject lh in lineHolders)
            {
                lh.SetActive(false);
            }

            placeIcon = false;
            draw = false;
            editGA = true;
            
        }
        
        else
        {
            if (widgetActive)
            {
                DrawMode();
            }

            foreach (GameObject lh in lineHolders)
            {
                lh.SetActive(true);
            }

            placeIcon = false;
            draw = false;
            editGA = false;

            buildSaveGAEdits.SavePrefabs();
        }

        editGABorder.SetActive(editGA);
    }

    public void eraseAllAnnotations()
    {
        foreach(GameObject lh in lineHolders)
        {
            for (int i = 0; i < lh.transform.childCount; i++)
            {
                Destroy(lh.transform.GetChild(i).gameObject);
            }
        }
    }

    public void ToggleArtwork()
    {
        foreach(GameObject lh in lineHolders)
        {
            lh.SetActive(toggleAnnotations.toggled);
        }
    }

    public void ToggleGAEdits()
    {
        foreach (GameObject gah in gaHolders)
        {
            gah.SetActive(toggleGAEdits.toggled);
        }
    }

    public void ChangeButtonColor(Color newColor, bool state)
    {
        HideButtonGO.SetActive(state);
        hideButtonIcon.DOColor(newColor, 0.125f);
        hideButtonText.DOColor(newColor, 0.125f);
    }

    public void SetUpButtons()
    {
        hideButton.onClick.AddListener(HideDecks);
        drawButton.onClick.AddListener(DrawMode);
        toggleGAEditMode.onClick.AddListener(ToggleEditGAMode);
        //toggleGAOverlay.onClick.AddListener(ToggleGAEdits);

        highlighterBH.GetComponent<Button>().onClick.AddListener(delegate {
            toggleHighlighter(highlighterBH.GetComponent<Button>());
        });

        penBH.GetComponent<Button>().onClick.AddListener(delegate {
            toggleHighlighter(penBH.GetComponent<Button>());
        });

        eraserBH.GetComponent<Button>().onClick.AddListener(delegate {
            toggleHighlighter(eraserBH.GetComponent<Button>());
        });

        moveBH.GetComponent<Button>().onClick.AddListener(delegate {
            toggleHighlighter(moveBH.GetComponent<Button>());
        });
    }

    public Color HighLighterColor(Color initialColor)
    {
        float H, S, V;

        Color.RGBToHSV(initialColor, out H, out S, out V);

        S = S - 0.4f;

        initialColor = Color.HSVToRGB(H, S, V);

        initialColor.a = .75f;

        return initialColor;
    }

    public bool OverObject()
    {
        if (ExtendedStandaloneInputModule.GetPointerEventData(Input.touches[0].fingerId).pointerEnter != null)
        {
            if(ExtendedStandaloneInputModule.GetPointerEventData(Input.touches[0].fingerId).pointerEnter.gameObject == curDeckMap)
            {
                return true;
            }

            else
            {
                if(ExtendedStandaloneInputModule.GetPointerEventData(Input.touches[0].fingerId).pointerEnter.name.StartsWith("DeckMap"))
                {
                    curDeckMap = ExtendedStandaloneInputModule.GetPointerEventData(Input.touches[0].fingerId).pointerEnter.gameObject;
                }
                
                else
                {
                    curDeckMap = null;
                }

                LineRenderer = null;
                return false;
            }
        }

        else
        {
            curDeckMap = null;
            LineRenderer = null;

            return false;
        }
    }
}
